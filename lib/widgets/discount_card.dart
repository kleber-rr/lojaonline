import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:lojavirtual/core/core_util.dart';
import 'package:lojavirtual/models/cart_model.dart';

class DiscountCard extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: ExpansionTile(
        title: Text("Cupom de Desconto", 
          textAlign: TextAlign.start, 
          style: TextStyle(fontWeight: FontWeight.w500,
            color: Colors.grey[700])
            ),
            leading: Icon(Icons.card_giftcard),
            trailing: Icon(Icons.add),
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "Digite seu cupom"
                  ),
                  initialValue: CartModel.of(context).couponCode ?? "",
                  onFieldSubmitted: (text){
                    Firestore.instance.collection("coupons").document(text).get().then((docSnap){
                      if(docSnap.data != null) {
                        CartModel.of(context).setCoupon(text, docSnap.data["percent"]);
                        CoreUtils.mensagemSnack("Desconto de ${docSnap.data["percent"]}% aplicado.", true, Scaffold.of(context), context);
                      } else {
                        CartModel.of(context).setCoupon(null, 0);
                        CoreUtils.mensagemSnack("Cupom não existente.", false, Scaffold.of(context), context);
                      }
                    });
                  },
                ),
              )
            ],
      ),
    );
  }
}