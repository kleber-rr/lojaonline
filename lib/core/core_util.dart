import 'package:flutter/material.dart';

class CoreUtils {
  static void mensagemSnack(String text, bool success, ScaffoldState state, BuildContext context){
    state.showSnackBar(SnackBar(
        content: Text(
          text,
          style: TextStyle(fontSize: 20.0),
        ),
        backgroundColor: success ? Theme.of(context).primaryColor : Colors.redAccent,
        duration: Duration(seconds: 2),
      ));
  }
}